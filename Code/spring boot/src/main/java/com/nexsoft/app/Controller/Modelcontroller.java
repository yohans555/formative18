package com.nexsoft.app.Controller;

import com.nexsoft.app.model.Model;
import com.nexsoft.app.service.Modelservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class Modelcontroller {
    @Autowired
    Modelservice modelservice;


    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save", method = RequestMethod.POST, consumes = "application/json")
    public void add(@RequestBody Model model ){
        modelservice.save(model);
    }
}
