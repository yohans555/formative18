// component
import React from 'react';
import {send} from './EventHandler';
import {send2} from './EventHandler'
import ReactDOM from 'react-dom';

class FormComponent extends React.Component{

    render(){
        return (
            <div>
                <h1>Zodiac</h1>
                <input id="name" type = "text" placeholder = "Enter name" />
                <br />
                <br />
                <input id="date" type = "date" />
                <br />
                <br />
                <button onClick = {send} >Submit</button>
            </div>
        );
     }
  }

  export default FormComponent;