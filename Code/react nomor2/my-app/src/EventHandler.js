function getZodiac(dates) {
  let date = new Date()
  date.setFullYear(dates.substring(0,4),dates.substring(5,7),dates.substring(8,10));
  let month = date.getMonth();
  let zodiac;
  switch (month) {
    case 1 :
      if (date.getDay()<21) {
        zodiac="Capricorn"
      } else {
        zodiac="Aquarius"
      }
      break;
    case 2:
      if (date.getDay()<20) {
        zodiac="Aquarius"
      } else {
        zodiac="Pisces"
      }
      break;
    case 3:
      if (date.getDay()<21) {
        zodiac="Pisces"
      } else {
        zodiac="Aries" 
      }
      break;
    case 4:
      if (date.getDay()<20) {
        zodiac="Aries"
      } else {
        zodiac="Taurus"
      }
      break;
    case 5:
      if (date.getDay()<21) {
        zodiac="Taurus" 
      } else {
        zodiac="Gemini"
      }
      break;
    case 6:
      if (date.getDay()<22) {
        zodiac="Gemnini"
      } else {
        zodiac="Cancer"
      }
      break;
    case 7:
      if (date.getDay()<24) {
        zodiac="Cancer"
      } else {
        zodiac="Leo"
      }
      break;    
    case 8:
      if (date.getDay()<24) {
        zodiac="Leo"
      } else {
        zodiac="Virgo"
      }
      break;
    case 9:
      if (date.getDay()<23) {
        zodiac="Virgo"
      } else {
        zodiac="Libra"
      }
      break;
    case 10:
      if (date.getDay()<23) {
        zodiac="Libra"
      } else {
        zodiac="Scorpio"
      }
      break;
    case 11:
      if (date.getDay()<23) {
        zodiac="Scorpio"
      } else {
        zodiac="Sagitarius"
      }
      break;
    case 12:
      if (date.getDay()<21) {
        zodiac="Sagitarius"
      } else {
        zodiac="Capricorn"
      }
      break;    
    default:
      break;
  }return zodiac
}

export function send() {

  let zodiacs = getZodiac(document.getElementById("date").value)
  let options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify({
      name: document.getElementById("name").value,
      birthdate: document.getElementById("date").value,
      zodiac:zodiacs
      }
    ),
  };
  // api for making post requests
  try {
    let fetchs = fetch("http://localhost:8080/save", options);
  } catch (error) {
    console.log('error disini');
  }
  
}